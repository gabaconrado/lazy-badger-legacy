# Constants
LB_HOME=$HOME/.lazy-badger
LB_DIR=$HOME/.lazy-badger/bin
APP_NAME=lazy-badger
APP_PATH=$LB_DIR/$APP_NAME

# Check if it is not installed
if ! [ -f "$APP_PATH" ]; then
    echo "App not installed."
    exit 0
fi

echo "-> Starting uninstall"

echo "-> Removing directories"
rm -rf $LB_HOME

echo "-> Uninstall finished."
echo "Do not forget to remove app folder from your path."
