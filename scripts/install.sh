# Constants
LB_DIR=$HOME/.lazy-badger/bin
APP_NAME=lazy-badger
APP_PATH=$LB_DIR/$APP_NAME
SCRIPT=$(readlink -f "$0")
DIRNAME=`dirname "$SCRIPT"`

# Check if it is already installed
if [ -f "$APP_PATH" ]; then
    echo "App already installed."
    exit 0
fi

echo "-> Starting install"

echo "-> Generating directories"
mkdir -p $LB_DIR

echo "-> Linking the executable"
ln -s $DIRNAME/../src/entrypoint.py $APP_PATH

echo "-> Installation finished."
echo "Do not forget to add the app folder to your PATH:"
echo "export PATH=\"\$HOME/.lazy-badger/bin\""
